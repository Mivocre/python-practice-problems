def skating_day_message(month_num, day_num):
    statement = "World Ice Skating Day is coming up!"
    if month_num == 12:
        if day_num > 4:
            statement = "You just missed it. There's another next year!"
        elif day_num == 4:
            statement = "YAY! It's World Ice Skating Day!"

    return statement
