def sentiment(message:str):
    happy = ":-)"
    sad = ":-("
    if message.count(happy) == 0 and message.count(sad) == 0:
        return "none"
    elif message.count(happy) > message.count(sad):
        return "happy"
    elif message.count(happy) < message.count(sad):
        return "sad"
    else:
        return "unsure"
