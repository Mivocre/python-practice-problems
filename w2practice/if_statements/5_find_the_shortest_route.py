def shortest(route_1, route_2, route_3):
    if route_1 < route_2:
        if route_1 < route_3:
            return route_1
    elif route_2 < route_3:
        return route_2
    else:
        return route_3
