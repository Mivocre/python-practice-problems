def calories(entree_num, side_num, dessert_num, drink_num):
#solution without an if statement
    # entree = [0, 522, 399, 501]
    # side = [0, 130, 125, 72]
    # dessert = [0, 222, 391, 100]
    # drink = [0, 10, 8, 120]
    # cal_count = entree[entree_num] + side[side_num] + dessert[dessert_num] + drink[drink_num]
    entree = [522, 399, 501]
    side = [130, 125, 72]
    dessert = [222, 391, 100]
    drink = [10, 8, 120]
    cal_count = 0
    if entree_num != 0:
        cal_count += entree[entree_num - 1]
    if side_num != 0:
        cal_count += side[side_num - 1]
    if dessert_num != 0:
        cal_count += dessert[dessert_num - 1]
    if drink_num != 0:
        cal_count += drink[drink_num - 1]
    return cal_count
