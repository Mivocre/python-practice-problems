def find_missing_registrant(registrants:list, finishers:list):
    for index in range(len(finishers)):
        if registrants[index] not in finishers:
            return registrants[index]
    return finishers[(len(finishers))]
