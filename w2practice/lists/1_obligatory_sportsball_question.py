def half_time_scores(
    game_length:int,  # in minutes
    team_1_score_times:list,  # each value is seconds
    team_2_score_times:list   # each value is seconds
):
    total_scores = 0
    half_time_sec = game_length * 30
    for time in team_1_score_times:
        if(time < half_time_sec):
            total_scores += 1
    for time in team_2_score_times:
        if(time < half_time_sec):
            total_scores += 1
    return total_scores
