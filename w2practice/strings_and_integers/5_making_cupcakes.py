def profit(num_cupcakes, num_tubes_frosting, tubes_per_cupcake):
    frost = num_tubes_frosting // tubes_per_cupcake
    unfrost = num_cupcakes - frost
    leftover = num_tubes_frosting % tubes_per_cupcake
    profit = (frost * 10) + (unfrost * 4) + leftover
    return profit
# should write a catch for division by zero
