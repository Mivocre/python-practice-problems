def new_hope(num_fars):
    result = "A long time ago in a galaxy"
    if num_fars == 1:
        result += " far"
    else:
        for count in range(num_fars - 1):
            result += " far,"
        result += " far"

    result += " away..."
    return result
