def cash_on_hand(expenses:list):
    cash = 0
    for expense in expenses:
        cash += 30
        cash -= expense
    cash += 30
    return cash
