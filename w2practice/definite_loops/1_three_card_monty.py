def monty(swaps:str):
    cards = [0, 1, 0]
    for char in swaps:
        if char == "L":
            temp = cards[0]
            cards[0] = cards[1]
            cards[1] = temp
        if char == "O":
            temp = cards[2]
            cards[2] = cards[0]
            cards[0] = temp
        if char =="R":
            temp = cards[1]
            cards[1] = cards[2]
            cards[2] = temp
    if cards[0] == 1:
        return "left"
    elif cards[1] == 1:
        return "middle"
    else:
        return "right"
