def valid_password(password:str):
    upper = 0
    lower = 0
    digit = 0
    if len(password) < 8 or len(password) > 12:
        return "bad"
    for char in password:
        if char.isupper():
            upper += 1
        elif char.islower():
            lower += 1
        elif char.isdigit():
            digit += 1
    if upper >= 3:
        if lower >= 2:
            if digit >= 2:
                return "good"
    return "bad"
