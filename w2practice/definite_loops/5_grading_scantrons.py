def grade_scantron(submission, answer_key):
    numcorrect = 0
    for index in range(len(answer_key)):
            if str(submission[index]) == str(answer_key[index]):
                numcorrect += 1
    return numcorrect
