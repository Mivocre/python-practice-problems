def language(text:str):
    german = 0
    english = 0
    for index in range(len(text) - 1):
        if f"{text[index]}{text[index+1]}" == "ei":
            german += 1
        elif f"{text[index]}{text[index+1]}" == "ie":
            english += 1
    if german > english:
        return "German"
    elif english > german:
        return "English"
    else:
        return "Maybe French?"

# Solution without manual looping

# def language(text:str):
#     if text.count("ei") > text.count("ie"):
#         return "German"
#     elif text.count("ie") > text.count("ei"):
#         return "English"
#     else:
#         return "Maybe French?"
