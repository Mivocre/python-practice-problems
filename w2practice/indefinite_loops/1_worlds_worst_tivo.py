def calculate_playlist(button_pushes:list):
    playlist = ["A", "B", "C", "D", "E"]
    if len(button_pushes) == 0:
        return playlist
    for button in button_pushes:
        if button == 1:
            temp = playlist[0]
            playlist[0] = playlist[1]
            playlist[1] = temp
        elif button == 2:
            temp = playlist[0]
            playlist.remove(playlist[0])
            playlist.append(temp)
        elif button == 3:
            temp = playlist[4]
            playlist.remove(playlist[4])
            playlist.insert(0, temp)
    return playlist
