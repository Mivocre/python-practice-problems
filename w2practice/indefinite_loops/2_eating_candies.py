def nia_eat(favorite_color, candy):
    time = 0
    nonfavcount = 0
    for piece in candy:
        if piece == favorite_color:
            time += 23
        else:
            nonfavcount = 1
    if nonfavcount == 1:
        time += 17
    return time




#solution without looping

# def nia_eat(favorite_color:str, candy:list):
#     time = 0
#     nonfavcount = len(candy) - candy.count(favorite_color)
#     if nonfavcount != 0:
#         time += 17
#     time += (candy.count(favorite_color) * 23)
#     return time
