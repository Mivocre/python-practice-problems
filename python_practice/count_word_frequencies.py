def count_word_frequencies(sentence:str):
    result = {}
    if sentence == "":
        return result
    templist = sentence.split(" ")
    for word in templist:
        result[word] = 0
    for word in templist:
        result[word] += 1


        #better way, only loops through sentence once
    # for word in templist:
    #     if word in result:
    #         result[word] += 1
    #     else:
    #         result[word] = 1



    return result
