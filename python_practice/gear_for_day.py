def gear_for_day(is_workday, is_sunny):
    gearlist = []
    if is_workday and not is_sunny:
        gearlist.append("umbrella")
    if is_workday:
        gearlist.append("laptop")
    else:
        gearlist.append("surfboard")
    return gearlist
