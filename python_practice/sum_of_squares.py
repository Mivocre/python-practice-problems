def sum_of_squares(values):
    if len(values) == 0:
        return None
    sum = 0
    for value in values:
        sum += value **2
    return sum
