def shift_letters(word:str):
    result = ""
    for letter in word:
        if ord(letter) == 90:
            result += chr(65)
        elif ord(letter) >= 65 and ord(letter) < 90:
            result += chr(ord(letter) + 1)
        elif ord(letter) == 122:
            result += chr(97)
        elif ord(letter) >= 97 and ord(letter) < 122:
            result += chr(ord(letter) + 1)
    return result
