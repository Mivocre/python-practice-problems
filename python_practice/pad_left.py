def pad_left(number, length, pad):
    if len(str(number)) - 1 == length:
        return str(number)
    result = ""
    for num in range(0, length - 1):
        if len(result) + len(str(number)) == length:
            result = result + str(number)
            return result
        else:
            result = result + str(pad)
    result = result + str(number)
    return result
