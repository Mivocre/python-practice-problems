# def remove_duplicate_letters(s:str):
    # if len(s) == 0:
    #     return s
    # newword = ""
    # for char in s:
    #     if char not in newword:
    #         newword = newword + newword.join(char)
    # return newword

# print(remove_duplicate_letters("aaaaaaaaaaaaaaaaa"))
# print(remove_duplicate_letters("abcdefg"))
# print(remove_duplicate_letters("akdlsidjalfkajdladksj"))


def remove_duplicate_letters(s:str):
    """removes duplicate letters in a string"""
    # seen = []
    seen = ""
    for letter in s:
        if letter not in seen:
            seen += letter
            # seen.append(letter)
    return seen
    # return "".join(seen)

#doing this problem using a dictionary
# def remove_duplicate_letters(s:str):
#     seen = {}
#     for letter in s:
#         seen[letter] = True
#     return "".join(seen.keys())


print(remove_duplicate_letters("aaaaaaaaaaaaaaaaa"))
print(remove_duplicate_letters("abcdefg"))
print(remove_duplicate_letters("akdlsidjalfkajdladksj"))
