def only_odds(nums:list):
    result = []
    for num in nums:
        if num % 2 == 1:
            result.append(num)
    return result
