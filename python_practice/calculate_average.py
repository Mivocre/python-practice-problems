def calculate_average(values: list):
    sum = 0
    if len(values) == 0:
        return None
    for value in values:
        sum += value
    return sum / len(values)
