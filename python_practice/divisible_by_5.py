def is_divisible_by_5(number):
    if number % 5 == 0:
        return "buzz"
    else:
        return number
