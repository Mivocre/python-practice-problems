def count_letters_and_digits(s:str):
    sum_digit = 0
    sum_alpha = 0
    if s == "":
        return 0, 0
    for char in s:
        if char.isdigit():
            sum_digit += 1
        if char.isalpha():
            sum_alpha += 1
    return sum_alpha, sum_digit
