def is_palindrome(word: str) -> bool:
    charlist = []
    reverselist = []
    for letter in word:
        charlist.append(letter)
    for index in range(len(charlist) - 1, -1, -1):
        reverselist.append(charlist[index])
    if charlist == reverselist:
        return True
    else:
        return False
