def pairwise_add(list1, list2):
    result = []
    for index in range(len(list1)):
        result.append(list1[index] + list2[index])
    return result
