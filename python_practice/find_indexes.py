def find_indexes(search_list:list, search_term):
    result = []
    for index in range(len(search_list)):
        if search_list[index] == search_term:
            result.append(index)
    return result
