def calculate_sum(values):
    if len(values) == 0:
        return None
    sum = 0
    for value in values:
        sum += value
    return sum
