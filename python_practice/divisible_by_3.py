def is_divisible_by_3(number: int):
    testvalue = number % 3
    if testvalue == 0:
        return "fizz"
    else:
        return number
# you can have a functino return more than one type
