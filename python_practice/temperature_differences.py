def temperature_differences(highs:list, lows:list):
    result = []
    for index in range(len(highs)):
        result.append(highs[index] - lows[index])
    return result
