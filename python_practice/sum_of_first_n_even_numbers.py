def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    if n == 0:
        return 0
    sum = 0
    for value in range(0, n + 1):
        sum += value * 2
    return sum
