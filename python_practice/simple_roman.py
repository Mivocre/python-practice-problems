def simple_roman(input:int):
    if input == 1:
        return "I"
    elif input == 2:
        return "II"
    elif input == 3:
        return "III"
    elif input == 4:
        return "IV"
    elif input == 5:
        return "V"
    elif input == 6:
        return "VI"
    elif input == 7:
        return "VII"
    elif input == 8:
        return "VIII"
    elif input == 9:
        return "IX"
    elif input == 10:
        return "X"
