def max_in_list(values):
    if len(values) == 0:
        return None
    max = 0
    for value in values:
        if value >= max:
            max = value
    return max
