def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    if x <= rect_x + rect_width and x >= rect_x:
        if y <= rect_y + rect_height and y >= rect_y:
            return True
        else:
            return False
    else:
        return False
