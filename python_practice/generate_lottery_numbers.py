from random import randint
def generate_lottery_numbers():
    result = []
    while len(result) < 6:
        num = randint(1, 40)
        if num not in result:
            result.append(num)
    return result
