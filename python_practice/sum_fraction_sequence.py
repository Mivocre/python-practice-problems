def sum_fraction_sequence(num:int):
    sum = 0
    for index in range(0, num):
        sum += (index + 1) / (index + 2)
    return sum
