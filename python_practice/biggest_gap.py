def biggest_gap(nums:list):
    gap = 0
    for index in range(len(nums) - 1):
        if abs(nums[index] - nums[index + 1]) > gap:
            gap = abs(nums[index] - nums[index + 1])
    return gap
