def reverse_dictionary(dictionary):
    dictkeys = []
    dictvalue = []
    reversedict = {}
    for index in dictionary:
        dictvalue.append(dictionary[str(index)])
        dictkeys.append(index)
    for index in range(len(dictkeys)):
        reversedict[dictvalue[index]] = dictkeys[index]
    return reversedict
