import math
def safe_divide(numer:float, denom:float):
    if denom == 0:
        return math.inf
    else:
        return numer/denom
