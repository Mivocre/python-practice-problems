def remove_duplicates(values:list):
    result = []
    for value in values:
        if value not in result:
            result.append(value)
    return result
