def group_cities_by_state(cities:list):
    result = {}
    for city in cities:
        citystate = city.split(",")
        if result.get(str(citystate[1]).strip()) == None:
            result[str(citystate[1]).strip()] = []
            result[str(citystate[1]).strip()].append(citystate[0].strip())
        else:
            result[str(citystate[1]).strip()].append(citystate[0].strip())
    return result
