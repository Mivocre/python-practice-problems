
# make a function for every condition to make the function run smoother

# It must have at least one lowercase letter (a-z)
def haslower(password: str):
    for letter in password:
        if letter.islower():
            return True
    return False

# It must have at least one uppercase letter (A-Z)
def hasupper(password: str):
    for letter in password:
        if letter.isupper():
            return True
    return False

# It must have at least one digit (0-9)
def hasdigit(password:str):
    for char in password:
        if char.isdigit():
            return True
    return False

# It must have at least one special character $, !, or @
def hasspecial(password:str):
    for char in password:
        if char in "$!@":
            return True
    return False

# It must have twelve or fewer characters in it
def correctlength(password:str):
    if len(password) >= 6 and len(password) <= 12:
        return True
    return False

def check_password(password: str) -> bool:
    if haslower(password) and hasupper(password) and hasdigit(password) and hasspecial(password) and correctlength(password):
        return True
    return False
