def halve_the_list(input:list):
    resultone = []
    resulttwo = []
    if len(input) % 2 == 1:
        for index in range(0, len(input) // 2 + 1):
            resultone.append(input[index])
        for index in range(len(input) // 2 + 1, len(input)):
            resulttwo.append(input[index])
    else:
        for index in range(0, len(input) // 2):
            resultone.append(input[index])
        for index in range(len(input) // 2, len(input)):
            resulttwo.append(input[index])

    return resultone, resulttwo
