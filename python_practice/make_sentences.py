def make_sentences(subjects:list, verbs:list, objects:list) -> list:
    result = []
    for sub in subjects:
        for verb in verbs:
            for obj in objects:
                result.append(sub + " " +  verb + " " + obj)
    return result
