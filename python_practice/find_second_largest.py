def find_second_largest(values):
    if len(values) == 0 or len(values) == 1:
        return None
    sortedvalues = values.sort()
    return values[len(values) - 2]
