def add_csv_lines(csv_lines: list) -> list:
    result = []
    for index in csv_lines:
        templist = index.split(",")
        tempsum = 0
        for temp in templist:
            tempsum += int(temp)
        result.append(tempsum)
    return result
