def translate(key_list, dictionary):
    result = []
    for keys in key_list:
        if dictionary.get(str(keys)):
            result.append(dictionary.get(str(keys)))
        else:
            result.append(None)
    return result
