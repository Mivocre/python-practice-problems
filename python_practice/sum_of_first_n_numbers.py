def sum_of_first_n_numbers(limit: int):
    sum = 0
    if limit == 0:
        return 0
    if limit < 0:
        return None
    for value in range(0, limit + 1):
        sum += value
    return sum
