def calculate_grade(values):
    if len(values) == 0:
        return None
    sum = 0
    for value in values:
        sum += value
    grade = sum / len(values)
    if grade >= 90:
        return "A"
    elif grade >= 80:
        return "B"
    elif grade >= 70:
        return "C"
    elif grade >= 60:
        return "D"
    else:
        return "F"
